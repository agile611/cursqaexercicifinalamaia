package com.itnove.crm.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by guillem on 01/03/16.
 */
public class EditAccountPage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[1]/h2")
    public WebElement titulo;

    @FindBy(xpath = ".//*[@id='tab-actions']/a")
    public WebElement actionsMenu;

    @FindBy(id = "delete_button")
    public WebElement deleteButton;

    public String getTitulo(){
        return titulo.getText();
    }

    public void deleteAccount(WebDriver driver, Actions hover) throws InterruptedException {
        hover.moveToElement(actionsMenu)
                .moveToElement(actionsMenu)
                .click().build().perform();
        hover.moveToElement(actionsMenu)
                .moveToElement(deleteButton)
                .click().build().perform();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        driver.switchTo().defaultContent();
    }

    public EditAccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
